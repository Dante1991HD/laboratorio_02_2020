#!/usr/bin/python
# -*- coding: utf-8 -*-


import json
# Extrae datos del archivo y los convierte en lista sobre lista


def genera_datos():

    archivo = open("top50.csv")
    datos = []
    for i, linea in enumerate(archivo):
        linea = linea.replace('\n', '').split(',')
        for j in linea[4:14]:
            if i > 0:
                int(j)
        datos.append(linea)
    archivo.close()
    return datos


# Calcula la cantidad de perteciones de una artista en la lista


def cantantes_repetidos():

    cantantes = {}
    artista = set([x[2] for x in datos[1:51]])
    print("artistas en el top 50:")
    for i in artista:
        print(i)
    for i in datos:
        repeticiones = sum(x.count(i[2]) for x in datos)
        if repeticiones > 1:
            cantantes.update({i[2]: repeticiones})
    print("La cantidad de cantantes es de:", len(set(artista)))
    for key, value in cantantes.items():
        if value == max(cantantes.values()):
            print("El cantante con mas canciones en el ranking es: {0},"
                  " con: {1} repeticiones".format(key, value))


# retorna la mediana de decibeles


def calculo_mediana():
    decibel = [int(x[7]) for x in datos[1:51]]
    decibel.sort()
    a = int(len(decibel)/2) - 1
    b = int(len(decibel)/2 + 1) - 1
    mediana = (decibel[a] + decibel[b])/2
    return mediana


# imprime los datos de las filas que se asemejan en su valor de decibeles


def mediana_decibeles():
    print("Dos datos que se ajustan a la mediana son:\n")
    for x in datos[1:51]:
        print("{0}".format(x[1:4])) if int(x[7]) == calculo_mediana() else 0


# imprime las canciones más bailables


def mas_bailables():
    bailable = [int(x[6]) for x in datos[1:51]]
    bailable.sort()
    print("Los géneros mas bailables")
    for x in datos[1:51]:
        print("Género:", x[3]) if int(x[6]) in bailable[47:51] else 0


# imprime las más lentas según ejercicio


def mas_lentas():
    lenta = [int(x[4]) for x in datos[1:51]]
    lenta.sort()
    print("\nLas canciones mas lentas son:")
    for x in datos[1:51]:
        print('Canción', x[1]) if int(x[4]) in lenta[0:3] else 0


# imprime las más ruidosas según el ejercicio


def popular_ruidosa():
    popularidad = [int(x[13]) for x in datos[1:51]]
    popularidad.sort()
    popularuidoso = False
    for x in datos[1:51]:
        if int(x[13]) == popularidad[len(popularidad) - 1]:
            if int(x[7]) < calculo_mediana():
                print("La canción mas popular y menos ruidosa", 
                      x[2], x[1], x[13], x[7])
                popularuidoso = True
    if not popularuidoso:
        print("NO")

# genera el archivo json en base a un diccionario


def save_file():
    print("Datos guardados en Top50.json\n")
    dictop50 = dict([[y, [x[datos[0].index(y)] 
                          for x in datos[1:51]]] for y in datos[0]])
    with open("Top50.json", 'w') as file:
        json.dump(dictop50, file)


def menu():
    texto = "1.respuestas ejercicio a\n2.respuestas ejercicio" \
            " b\n3.respuestas ejercicio c\n" \
            "4.respuestas ejercicio d\n5.respuestas ejercicio e\n6.Salir\n"
    print("Seleccione un número en el menú")
    while True:
        try:
            seleccion = int(input(texto))
            if seleccion == 1:
                cantantes_repetidos()
            if seleccion == 2:
                mediana_decibeles()
            if seleccion == 3:
                mas_bailables()
                mas_lentas()
            if seleccion == 4:
                popular_ruidosa()
            if seleccion == 5:
                save_file()
            if seleccion == 6:
                exit("Puede revisar su trabajo en Top50.json, Gracias.")
        except ValueError:
            print("\nIngrese un número en el menú por favor")
        else:
            print("Seleccione un número en el menú")

# main llama a menú y comienza el programa
if __name__ == '__main__':

    datos = genera_datos()
    menu()
